**Docker Summary**

**What is Docker?**

Docker is a program for developers to develop, and run applications with containers.

**Docker Terminology:**
1. Docker Image:
A docker image contains code,runtime,libraries,environment variables,configuration files  which are required to run an application as a container
2. Container:
A Docker container is a running Docker image.
3. Docker Hub:
It is a hub for images and containers.

**Docker Basic Commands:**
1. docker ps:
The docker ps command allows us to view all the containers that are running on the Docker Host.`$ docker ps`
2. docker start:
This command starts any stopped containers.  `$ docker start 30986`
3. docker stop:
This command stops any running containers.  `$ docker stop 30986`
4. docker run:
This command creates containers from docker images.  `$ docker run sneha`
5. docker rm: 
This commands removes or deletes containers.  `$ docker rm sneha`

**Docker Workflow:**
1. Downloading the docker.  `docker pull snehabhapkar/trydock`
2. Running the docker.  `docker run -ti snehabhapkar/trydock /bin/bash`
3. Accessing docker terminal.  `docker cp hello.py e0b72ff850f8:/`
4. Installing additional required dependencies.  `docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh`
5. Compiling and running the code inside docker.  `docker exec e0b72ff850f8 python3 hello.py`
6. Commiting files to docker.  `docker commit e0b72ff850f8 snehabhapkar/trydock`
7. Pushing docker image to docker hub.  `docker push username/repo`

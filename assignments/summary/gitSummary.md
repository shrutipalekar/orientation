**Git Lab Summary**

**What is Git?**  
Git is a version controlled software used to develop software along with the teammates.
Git keeps tracks of all the changes mae by every team member in the software.

**Terminology:** 
1. Commit:
The work done on the software is saved on the local machine.
2. Push:
Pushing is syncing your commits to the remote repository.
3. Branch:
The branch are separate small instances of code from the Master or main branch.
4. Merge:
The bug free branch is merged or integrated into the master branch.
5. Clone:
Clone is a copy of online repository to the local machine.
6. Fork:
Forking is getting the entire repository under one's own name.

**Git Internals:**

Git has three main states of files:
1. Modified: File has been changed but not committed to the repository.
2. Staged: Marked modifies file in its current version to go into next commit.
3. Committed:Date is stored in local repository.

**Git Workflow:**
1. Cloning the repository
    `$ git clone <link-to-repository> `  
2. Creating new branch
    `$ git checkout master
    $ git checkout -b <your-branch-name>`
3. Modify files in working tree
4. Selectively staging changes to add to next commit.
    `$ git add` 
5. Committing files to local repository
    `$ git commit -sv`
6. Pushing the files to remote repository
    `$ git push origin <branch-name>`